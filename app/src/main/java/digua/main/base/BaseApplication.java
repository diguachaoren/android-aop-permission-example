package digua.main.base;

import android.app.Application;

import digua.main.utils.DG_Toast;


/**
 * Created by digua on 2015/10/27.
 */
public class BaseApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();

        // 初始化Toast
        DG_Toast.init(this);

    }


}
