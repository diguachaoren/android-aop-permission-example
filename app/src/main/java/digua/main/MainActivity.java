package com.digua;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;



import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import digua.main.R;
import digua.main.page.Module0;
import digua.main.page.Module1;
import digua.main.page.Module2;
import digua.main.utils.DG_Toast;
import digua.permission.Constants;
import digua.permission.MYEventMessage;

public class MainActivity extends Activity {

    @Bind(R.id.button1)
    Button button1;

    @Bind(R.id.button2)
    Button button2;

    @Bind(R.id.button11)
    Button button11;

    @Bind(R.id.button22)
    Button button22;

    @Bind(R.id.radioGroup)
    RadioGroup radioGroup;

    @Bind(R.id.radioMale)
    RadioButton radioMale;

    @Bind(R.id.radioFemale)
    RadioButton radioFemale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        //根据ID找到RadioGroup实例
        RadioGroup group = (RadioGroup)this.findViewById(R.id.radioGroup);
        //绑定一个匿名监听器
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                // TODO Auto-generated method stub
                int radioButtonId = arg0.getCheckedRadioButtonId();
                switch (radioButtonId){
                    case  R.id.radioMale:
                        DG_Toast.show("男");
                        Constants.ROLE = 1;
                        break;
                    case  R.id.radioFemale:
                        DG_Toast.show("女");
                        Constants.ROLE = 2;
                        break;
                }
            }
        });
    }

    @OnClick({R.id.button1, R.id.button2, R.id.button11, R.id.button22})
    public void clickMenuButton(View view) {

        switch (view.getId()) {
            case R.id.button1:
                jump();
                break;
            case R.id.button2:
                jumpToModule2();
                break;
            case R.id.button11:
                action1();
                break;
            case R.id.button22:
                action2();
                break;
        }
    }


    private void jump() {

    }


    // 跳转业务1
    private void jumpToModule1() {
        Log.e("====》", "【跳转业务】");
        Intent intent = new Intent(this, Module1.class);
        startActivity(intent);
    }

    // 跳转业务2
    private void jumpToModule2() {
        Intent intent = new Intent(this, Module2.class);
        startActivity(intent);
    }

    // 事件1
    private void action1() {
        DG_Toast.show("事件1");
    }

    // 事件2
    private void action2() {
        DG_Toast.show("事件2");
    }

    // 无权限访问
    private void jumpToModule0() {
        Log.e("====》", "【跳转业务】");
        Intent intent = new Intent(this, Module0.class);
        startActivity(intent);
    }

    @Subscriber
    public void onEventMainThread(MYEventMessage event) {
        if (event.getCode().equals("1")) {
            DG_Toast.show("男");
            jumpToModule1();
        } else if (event.getCode().equals("2")) {
            DG_Toast.show("女");
            jumpToModule2();
        }
    }


}
