package digua.main.utils;

import android.content.Context;
import android.view.Gravity;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;


/**
 * Created by digua on 2016/1/7.
 *
 * Toast 封装类
 *
 * 需要引入包：compile 'com.github.johnpersano:supertoasts:1.3.4@aar'
 */
public class DG_Toast {

    public static Context mContext;

    public static SuperToast mToast ;

    // 初始化Toast 并进行设置
    public static void init(Context context){
        // content
        mContext = context;
        // 颜色》BLACK BLUE GRAY GREEN ORANGE PURPLE RED WHITE
        mToast = new SuperToast(context, Style.getStyle(Style.GRAY));
        // 显示时间
        mToast.setDuration(1000);
        // 显示的动画效果=》 FADE,FLYIN, SCALE,POPUP
        mToast.setAnimations(SuperToast.Animations.FADE);
        //设置字体大小
        mToast.setTextSize(16);
        mToast.setGravity(Gravity.CENTER,0,0);
    }

    // 显示的信息
    public static void show(String msg){
        mToast.setText(msg);
        mToast.show();
    }


}
