package digua.permission;

/**
 * Created by 地瓜超人 on 2016/2/25.
 */
public class MYEventMessage {

    private String msg;
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
