package digua.permission;

import android.util.Log;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.simple.eventbus.EventBus;

/**
 * Created by 地瓜超人 on 2016/2/25.
 */
@Aspect
public class TestAspect {

    // action1
    // jump

    @Before("execution(* com.digua.MainActivity.action1())")
    public void check(){
        if(Constants.ROLE == Constants.A){
            MYEventMessage message = new MYEventMessage();
            message.setCode("1");
            message.setMsg("无权限");
            Log.e("------------>","无权限");
            EventBus.getDefault().post(message);
        }else if(Constants.ROLE == Constants.B){
            MYEventMessage message = new MYEventMessage();
            message.setCode("2");
            message.setMsg("有权限");
            Log.e("------------>","有权限");
            EventBus.getDefault().post(message);
        }
    }


    @Before("execution(* com.digua.MainActivity.jump())")
    public void check2(){
        if(Constants.ROLE == Constants.A){
            MYEventMessage message = new MYEventMessage();
            message.setCode("1");
            message.setMsg("无权限");
            Log.e("------------>","无权限");
            EventBus.getDefault().post(message);
        }else if(Constants.ROLE == Constants.B){
            MYEventMessage message = new MYEventMessage();
            message.setCode("2");
            message.setMsg("有权限");
            Log.e("------------>","有权限");
            EventBus.getDefault().post(message);
        }
    }

}
